import random

from personne import Personne

class Salarie(Personne):

    def __init__(self, personnes_nom, personnes_prenom, identifiant_personnes,
                 salaire_mensuel, salarie_voiture_fonction=None, salarie_voiture_personnelles=None, salarie_bureau=None):
        Personne.__init__(self, personnes_nom, personnes_prenom, identifiant_personnes)

        self.salaire_mensuel =salaire_mensuel
        self.salarie_voiture_fonction =salarie_voiture_fonction
        self.salarie_voiture_personnelles =salarie_voiture_personnelles
        self.salarie_bureau = salarie_bureau

    # cette fonction ajoute à lobjet salrie crée, la voiture dont il est proprietaire
    def affectation_voiture_fonction_salarie(self, voiture):
        self.salarie_voiture_fonction = voiture

    # cette fonction ajoute à lobjet salrie crée, la voiture dont il est proprietaire
    def affectation_voiture_personnelles(self, voiture):
        self.salarie_voiture_personnelles = voiture

    # cette fonction ajoute le salaire d'un salarie de  ma classe salarie
    def salaire_mensuels(self, value):
        self.salaire_mensuel = value

    # cette fonction renvoi le salaire d'un salarie
    def salarie_voiture_fonction(self):
        return self.salarie_voiture_fonction

    # cette fonction affecte une voiture  à un salarie
    def salarie_voiture_fonctions(self, value):
        self.salarie_voiture_fonction = value


    def salarie_voiture_personnelle(self):
        return self.salarie_voiture_personnelles


    def salarie_voiture_personnelles(self, value):
        self.salarie_voiture_personnelles = value
