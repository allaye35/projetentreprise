from bienEntreprises import Bien_entreprise

class Bureau(Bien_entreprise):

    def __init__(self, identifiant, prix_de_vente,  prix_achat):
        Bien_entreprise.__init__(self, identifiant, prix_de_vente, prix_achat)

    # cette fonction ajoute à lobjet bureau crée, l'entreprise proprietaire
    def affectation_bureau_Entreprise(self, entreprise):

        self.proprietaire= entreprise

    #cette fonction verifier si l'objet bureau a un proprietaire
    def situation_bureau_libre_ou_occupe(self):

        if self.proprietaire != None :

            return True

        else:

            return False

