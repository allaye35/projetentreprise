import csv
import names
import random
from bureau import Bureau
from directeur import Directeur
from entreprise import Entreprise
from salarie import Salarie
from voiture import Voiture
from graph import BaseGraph
from sauvegarder import Enregistrement

#creation de la classe simulation, qui contiendra toutes mes objets instanciers
class Simulation():
    def __init__(self):
        self.listes_salaries = []
        self.listes_voitures = []
        self.listes_bureaux = []
        self.listes_entreprises = []
        self.liste_directeur = None
        self.charge_mensuel_totales=[]
        self.liste_mois = []
    #je crée mon objet salarie et je l'ajoute dans ma liste  de mes salaries
    def creation_salaries(self, nombre_de_salarie):
        for i in range(nombre_de_salarie):
            rand_firstname = names.get_first_name(gender='male')
            rand_lastname = names.get_last_name()
            rand_identifiant = random.randint(1, nombre_de_salarie + 1)
            rand_salaire_mensuel = random.randint(1300, 3000)

            salarie = Salarie(rand_firstname, rand_lastname, rand_identifiant, rand_salaire_mensuel)
            self.listes_salaries.append(salarie)
            return self.listes_salaries

    def enregistrer_salaries(self):
            fichier = open("annuaire.csv", "wt")
            ecrivainCSV = csv.writer(fichier,delimiter=";")
            for listes_salaries in self.listes_salaries:
                ecrivainCSV.writerow(["personnes_nom", "personnes_prenom", "identifiant_personnes", "salaire_mensuel"])
                ecrivainCSV.writerow([listes_salaries])
            fichier.close()

    #je crée mon objet voitures et je lui donne un proprietaire, puis je l'ajoute dans la liste de mes voitures
    def creation_voitures(self, nombre_de_voiture):
        for i in range(nombre_de_voiture):
            rand_prix_vente = random.randint(1500, 10000)
            print(rand_prix_vente)
            rand_proprietaire = random.choice(self.listes_salaries)
            print(rand_proprietaire)
            rand_identifiant_voiture = random.randint(1, nombre_de_voiture + 1)
            print(rand_identifiant_voiture)
            rand_prix_achat = random.randint(15000, 40000)
            print(rand_prix_achat)
            rand_cout_entretient = random.randint(1500, 10000)
            print(rand_cout_entretient)
            voiture = Voiture(rand_identifiant_voiture, rand_prix_vente,
                              rand_prix_achat, rand_proprietaire, rand_cout_entretient)

            rand_proprietaire.affectation_voiture_fonction_salarie(voiture)
            self.listes_voitures.append(voiture)

    def enregistrer_voitures(self):
        fichier = open("annuaire.csv", "wt")
        ecrivainCSV = csv.writer(fichier, delimiter=";")
        for listes_voitures in self.listes_voitures:
            ecrivainCSV.writerow(['identifiant_voiture', 'prix_vente', 'prix_achat', 'proprietaire', "cout_entretient"])
            ecrivainCSV.writerow([listes_voitures])
        fichier.close()


    #je crée mon objet directeur et je l'affcte a mon attribut
    def creation_directeur(self):
            rand_directeur_firstname = names.get_first_name(gender='male')
            rand_directeur_lastname = names.get_last_name()
            rand_directeur_identifiant = names.get_first_name(gender='male')
            rand_salaire_mensuel_directeur = random.randint(3000, 10000)

            directeur = Directeur(rand_directeur_firstname, rand_directeur_lastname,
                                  rand_directeur_identifiant, rand_salaire_mensuel_directeur)
            self.liste_directeur = directeur

    def enregistrement_directeur(self):
        fichier = open("annuaire.csv", "wt")
        ecrivainCSV = csv.writer(fichier, delimiter=";")
        ecrivainCSV.writerow(["directeur_nom", "directeur_prenom", "identifiant_directeur"])
        ecrivainCSV.writerow([self.liste_directeur])
        fichier.close()

    #je crée mon objet bureau et je l'ajoute dans ma liste  de mes bureaux
    def creation_bureaux(self, nombre_de_bureaux):
        for i in range(nombre_de_bureaux):
            rand_prix_vente = random.randint(1500, 4000)
            rand_identifiant_bureau = random.randint(1, nombre_de_bureaux + 1)
            rand_prix_achat = random.randint(15000, 40000)
            bureaux = Bureau(rand_identifiant_bureau, rand_prix_vente, rand_prix_achat)
            self.listes_bureaux.append(bureaux)


    def enregistrement_bureau(self):
        fichier = open("annuaire.csv", "wt")
        ecrivainCSV = csv.writer(fichier, delimiter=";")
        for bureaux in self.listes_bureaux:
            ecrivainCSV.writerow(['identifiant_bureau', 'prix_vente', 'prix_achat'])
            ecrivainCSV.writerow([bureaux])
        fichier.close()


    """je crée mon objet entreprise et je l'ajoute toutes les liste d'objet precedenment crée: salarie, voiture, bureau,
     puis a mon objet entreprise, je récupère mes charge mensule totale pour un mois, dans ma classe simulation"""

    def creation_entreprises(self):

        rand_identifiant_entreprise = random.randint(1, 100)
        print( "rand_identifiant_entreprise", rand_identifiant_entreprise)
        rand_directeur = self.liste_directeur
        print("rand_directeur", rand_directeur)
        rand_voiture_des_fonction = self.listes_voitures
        print("rand_voiture_des_fonction", rand_voiture_des_fonction)
        rand_ca_mensuel = random.randint(10000, 100000)
        print("rand_ca_mensuel", rand_ca_mensuel)
        rand_bureaux_prorietaire = random.choice(self.listes_bureaux)
        print("rand_bureaux_prorietaire", rand_bureaux_prorietaire)
        rand_listes_salaries = self.listes_salaries
        print("rand_listes_salaries", rand_listes_salaries)
        entreprise = Entreprise(rand_identifiant_entreprise, rand_directeur,
                                rand_voiture_des_fonction, rand_ca_mensuel, rand_bureaux_prorietaire,
                                rand_listes_salaries)

        rand_bureaux_prorietaire.affectation_bureau_Entreprise(entreprise)
        entreprise.affectation_Entreprise_bureau(rand_bureaux_prorietaire)

        entreprise.embauche(self.listes_salaries)
        entreprise.achat_voiture_fonction(self.listes_voitures)
        entreprise.achat_bureau(self.listes_bureaux)
        entreprise.directeur_entreprise(self.liste_directeur)
        self.listes_entreprises.append(entreprise)
        print("self.charge_mensuel_totales", self.charge_mensuel_totales)
        self.charge_mensuel_totales.append(entreprise.charger_mensuel_totale())
        print("self.charge_mensuel_totales", self.charge_mensuel_totales)


    def enregistrement_entreprise(self):
        fichier = open("annuaire.csv", "wt")
        ecrivainCSV = csv.writer(fichier, delimiter=";")
        for entreprise in self.listes_entreprises:
            ecrivainCSV.writerow(['identifiant_entreprise', 'directeur', 'voiture_des_fonction', 'ca_mensuel',
                                  "listes_salaries"])
            ecrivainCSV.writerow([entreprise])
        fichier.close()

    #je fais  ma representation graphique de mes charges mensuels en fonction du nombre de mois à simuler
    def representation_graphique(self):
        graphe = BaseGraph()
        print("self.charge_mensuel_totales", self.charge_mensuel_totales)
        graphe.show(self.charge_mensuel_totales, self.liste_mois)
        print("self.charge_mensuel_totales", self.charge_mensuel_totales)
        print("self.charge_mensuel_totales", self.liste_mois)

    """jappelle l'ensemble de mes  fonctions qui créent mes objets, en leurs passant les arguments, en fonction du nombre 
    de mois a simuler"""
    def nombre_mois_simuler(self, salaries, voitures, bureaux, nombre_de_mois):
        for mois in range(1, nombre_de_mois + 1):
            self.liste_mois.append(mois)
            self.creation_salaries(salaries)
            self.creation_voitures(voitures)
            self.creation_directeur()
            self.creation_bureaux(bureaux)
            self.creation_entreprises()
            self.enregistrer_salaries()
            self.enregistrer_voitures()
            self.enregistrement_directeur()
            self.enregistrement_bureau()
            self.enregistrement_entreprise()
            print("self.liste_mois", self.liste_mois)

    """"je lève une exception, si le nombre de mois n'est pas entre superieur ou egales 120 et superieur ou egales a 0, 
    et sinon j'appelle " ma fonction nombre de mois simuler """

    def simuler(self,salaries, voitures, bureaux, nombre_de_mois):

        try:
            if nombre_de_mois < 0 or nombre_de_mois >= 120:
                raise PeriodOutOfScopeError(nombre_de_mois)
            self.nombre_mois_simuler(salaries, voitures, bureaux, nombre_de_mois)
            self.representation_graphique()
        except PeriodOutOfScopeError as exc:
            print(PeriodOutOfScopeError.__doc__, " : ", exc)

#je créer une classe exception, qui contiendra les messages d'erreur
class PeriodOutOfScopeError(Exception):
    """
        vous avez demande une simulation sur un nombre de mois négatif, ou sur une durée
                    "de plus de 120 mois (10 ans)
      """