
import csv
class Enregistrement:
    def __init__(self):
        def enregistrement_salrie(self):
            fichier = open("annuaire.csv", "wt")
            ecrivainCSV = csv.writer(fichier, delimiter=";")
            ecrivainCSV.writerow(["personnes_nom", "personnes_prenom", "identifiant_personnes", "salaire_mensuel"])
            ecrivainCSV.writerow(
                [self.rand_firstname, self.rand_lastname, self.rand_identifiant, self.rand_salaire_mensuel])
            fichier.close()

        def enregistrement_directeur(self):
            fichier = open("annuaire.csv", "wt")
            ecrivainCSV = csv.writer(fichier, delimiter=";")
            ecrivainCSV.writerow(["directeur_nom", "directeur_prenom", "identifiant_directeur"])
            ecrivainCSV.writerow([self.rand_directeur_firstname, self.rand_directeur_lastname,
                                  self.rand_directeur_identifiant, self.rand_salaire_mensuel_directeur])
            fichier.close()

        def enregistrement_voiture(self):
            fichier = open("annuaire.csv", "wt")
            ecrivainCSV = csv.writer(fichier, delimiter=";")
            ecrivainCSV.writerow(['identifiant_voiture', 'prix_vente', 'prix_achat', 'proprietaire', "cout_entretient"])
            ecrivainCSV.writerow([self.rand_identifiant_voiture, self.rand_prix_vente,
                                  self.rand_prix_achat, self.rand_proprietaire, self.rand_cout_entretient])
            fichier.close()

        def enregistrement_entreprise(self):
            fichier = open("annuaire.csv", "wt")
            ecrivainCSV = csv.writer(fichier, delimiter=";")
            ecrivainCSV.writerow(['identifiant_entreprise', 'directeur', 'voiture_des_fonction', 'ca_mensuel',
                                  "listes_salaries"])
            ecrivainCSV.writerow([self.rand_identifiant_entreprise, self.rand_directeur,
                                  self.rand_voiture_des_fonction, self.rand_ca_mensuel, self.rand_bureaux_prorietaire,
                                  self.rand_listes_salaries])
            fichier.close()

        def enregistrement_bureau(self):
            fichier = open("annuaire.csv", "wt")
            ecrivainCSV = csv.writer(fichier, delimiter=";")
            ecrivainCSV.writerow(['identifiant_bureau', 'prix_vente', 'prix_achat'])
            ecrivainCSV.writerow([self.rand_identifiant_bureau, self.rand_prix_vente, self.rand_prix_achat])
            fichier.close()