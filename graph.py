import matplotlib.pyplot as plt

#cette classe crée ma representation graphique
class BaseGraph:

    def __init__(self):
        self.title = "Compte de Resultat"
        self.x_label = "mois"
        self.y_label = "charge mensuel"
        self.show_grid = True
        self.x_values = None
        self.y_values = None

    """cette fonction reçoit deux arguments, que represente sur laxe des abcises X le nombre de mois à simuler
    et sur les ordonnées Y mes charges mensuelles"""
    def show(self, charge_mensuels, liste_mois):
        self.x_values = liste_mois
        self.y_values = charge_mensuels
        plt.plot(self.x_values, self.y_values, '.')
        plt.xlabel(self.x_label)
        plt.ylabel(self.y_label)
        plt.title(self.title)
        plt.grid(self.show_grid)
        plt.show()

