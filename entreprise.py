
class Entreprise:

    def __init__(self, identifiant_entreprise, directeur,
                voiture_des_fonction, ca_mensuel, bureaux, listes_salaries):
        self.identifiant_entreprise= identifiant_entreprise
        self.directeur= directeur
        self.listes_voiture_des_fonction = voiture_des_fonction
        self.ca_mensuel = ca_mensuel
        self.listes_bureaux = [bureaux]
        print('self.listes_bureaux' , self.listes_bureaux)
        self.listes_salaries= listes_salaries
        self.liste_entreprise_proprietaire_bureaux = []
        self.charger_mensuel_totaless = 0
        self.charger_salariale_totale= 0
        self.charger_voiture_totale= 0
        self.charger_salaire_directeur = 0
        self.charger_bureaux_totale = 0

    # affectation de mon objet bureau à son proprietaire
    def affectation_Entreprise_bureau(self, bureaux):
        self.liste_entreprise_proprietaire_bureaux.append(bureaux)

    # ajout dans ma liste salarie  mes liste d'objet salaries instancier
    def embauche(self, salarie):
        self.listes_salaries = salarie

    # ajout d'un salaries supplementaire à ma liste salarie
    def embauche_salarie(self, salarie):
        self.listes_salaries.append(salarie)

    # ajout d'un directeur à une entreprise
    def directeur_entreprise(self, directeur):
        self.directeur = directeur

    # cette fonction permet de supprimer un salarie dans ma liste des salarie
    def licenciement(self, salarie_licencier):
        for salarie in self.listes_salaries:
            if salarie == salarie_licencier:
                self.listes_salaries.remove(salarie_licencier)
                self.charger_salariale_totale -= salarie_licencier.salaire_mensuel
                self.charger_mensuel_totale -= salarie_licencier


    # ajout dans ma liste voiture mes listes d'objets voitures instanciers
    def achat_voiture_fonction(self, voiture_des_fonction):
            self.listes_voiture_des_fonction =voiture_des_fonction

    """ cette fonction permet de supprimer la voiture choisis dans la liste des voiture, puis fait une soustraction 
    aux couts totals mensuels et au cout totale des voiture et en suite une addition 
       du prix de vente au chiffre d'affaire mensuel de l'entreprise.
        """
    def vente_voiture_de_fonction(self, voiture_vendu):
        for voiture in self.listes_voiture_des_fonction:
            if voiture_vendu ==  voiture:
                self.listes_voiture_des_fonction.remove(voiture_vendu)
                self.charger_voiture_totale -= voiture_vendu.prix_de_vente
                self.charger_mensuel_totale -= voiture_vendu.prix_de_vente
                self.ca_mensuel += voiture_vendu.prix_de_vente

    # ajout dans ma liste bureaux, mes listes d'objets voitures instanciers
    def achat_bureau(self, bureaux):
        print('self.liste_bureaux', type(self.listes_bureaux))
        self.listes_bureaux= self.listes_bureaux + bureaux
        print('self.liste_bureaux', self.listes_bureaux)

    """ cette fonction permet de supprimer le bureau choisis dans la liste des bureau, a condition que le bureau soit 
    libre, puis fait une soustraction aux couts totals mensuels et au cout totale des bureau et en suite une addition 
    du prix de vente au chiffre d'affaire mensuel de l'entreprise.
     """
    def vente_bureau(self, bureau_vendu):
        for bureaux in self.listes_bureaux:
            if bureaux == bureau_vendu and bureau_vendu.proprietaire==None:
                self.listes_bureaux.remove(bureau_vendu)
                self.charger_bureaux_totale -= bureau_vendu.prix_vente
                self.charger_mensuel_totale -= bureau_vendu.prix_de_vente
                self.ca_mensuel += bureau_vendu.prix_de_vente
            else:
                print("le bureau a un proprietaire et ne peut pas etre vendu")

    # cette fonction permet de calculer le cout salariale totale de l'entreprise
    def charger_salariale(self):
        charger_salariale_totales=0
        for salarie in self.listes_salaries:
            print("haha")
            print(charger_salariale_totales)
            print(salarie.salaire_mensuel)
            charger_salariale_totales = charger_salariale_totales + salarie.salaire_mensuel
            self.charger_salariale_totale = charger_salariale_totales
            print("self.charger_salariale_totales", self.charger_salariale_totale)

    # cette fonction permet de calculer le cout totale de l'ensemble des bureaux
    def charger_bureau(self):
        charger_bureaux_totale = 0
        for bureau in self.listes_bureaux:
            charger_bureaux_totale -= charger_bureaux_totale + bureau.prix_achat
            self.charger_bureaux_totale = charger_bureaux_totale
            print("self.charger_bureaux_totale", self.charger_bureaux_totale)
        return self.charger_bureaux_totale

    # cette fonction permet de calculer le cout total de l'ensemble des voitures
    def charger_voiture(self):
        charger_voiture_totale = 0
        print("charger_voiture_totale", charger_voiture_totale)
        for voiture_des_fonction in self.listes_voiture_des_fonction:
            charger_voiture_totale -= (charger_voiture_totale + voiture_des_fonction.prix_achat +
                                       voiture_des_fonction.cout_entretient_mensuel)
            self.charger_voiture_totale = charger_voiture_totale
            print("self.charger_voiture_totale", self.charger_voiture_totale)
        return self.charger_voiture_totale

    # cette fonction permet de calculer le salaire totale du directeur de l'entreprise
    def charger_directeur(self):
        self.charger_salaire_directeur =  self.directeur.salaire_mensuel
        print("self.charger_salaire_directeur", self.charger_salaire_directeur)
        return self.charger_salaire_directeur

    # cette fonction fait la somme totale de l'ensemble des charges de l'entreprise par mois
    def charger_mensuel_totale(self):
        self.charger_salariale()
        self.charger_bureau()
        self.charger_voiture()
        self. charger_directeur()
        charger_totale = (self.charger_voiture_totale + self.charger_salaire_directeur +
                         self.charger_bureaux_totale + self.charger_salariale_totale)

        self.charger_mensuel_totaless= charger_totale
        print("self.charger_mensuel_totale", self.charger_mensuel_totaless)
        return self.charger_mensuel_totaless