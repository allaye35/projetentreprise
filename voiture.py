from bienEntreprises import Bien_entreprise

"""cette classe est une classe fille de ma classe bien_entreprises, aussi elle une la classe parente de mes classes 
voiture_personnelle et voiture_de_fonction"""

class Voiture(Bien_entreprise):

    def __init__(self, identifiant, prix_de_vente,  prix_achat, proprietaire,  cout_entretient):
        Bien_entreprise.__init__(self, identifiant, prix_de_vente,  prix_achat)

        self.cout_entretient_mensuel = cout_entretient