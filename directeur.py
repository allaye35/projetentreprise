from salarie import Salarie

class Directeur(Salarie):

    def __init__(self, personnes_nom, personnes_prenom, identifiant_personnes, salaire_mensuel, directeur_entreprise = None,
                 salarie_voiture_fonction = None, salarie_voiture_personnelles = None, hausser_salariale =None):

        Salarie.__init__(self, personnes_nom, personnes_prenom, identifiant_personnes, salaire_mensuel,
                 salarie_voiture_fonction=None, salarie_voiture_personnelles = None, salarie_bureau = None)

        self.directeur_entreprise = directeur_entreprise
        self.hausser_salariale = None

    # cette fonction permet de retourne l'objet directeur
    def directeur_entreprise(self):
        return self.directeur_entreprise

    # cette fonction ajoute l'objet directeur à ma classe directeur
    def directeur_entreprises(self, value):
        self.directeur_entreprise = value

    """Cette fonction augmente le salaire des salarie de l'enreprise, à condition que cela soit faite par le directeur
     de l'entreprise, sinon une exception est levé et capter par un message d'erreur"""
    def augmentation_salaire(self, hausser_salariale):
        try:
            if not self.directeur_entreprise:
                raise AccessRightError(self.directeur_entreprise)
            self.hausser_salariale = hausser_salariale
            return self.hausser_salariale

        except AccessRightError as exc:
            print(AccessRightError.__doc__, " : ", exc)

    #cette fonction renvoi le salaire du directur de l'entreprise
    def salaire_mensuel_directeur(self):
        self.salaire_mensuel += self.hausser_salariale
        return self.salaire_mensuel

#cette classe est la classe d'exeception, et contient le mesage d'erreur
class AccessRightError(Exception):
    """
      la personne qui augmente le salaire doit etre le directeur
      """



